# Package sambamba version 0.7.1
Sambamba is a high performance, highly parallel, robust and fast tool (and library), written in the D programming language, for working with SAM and BAM files.
sambamba Version: 0.7.1
https://github.com/biod/sambamba

Package installation using Miniconda3 V4.7.12 All packages are in /opt/miniconda/bin & are in PATH sambamba Version: 0.7.1
Singularity container based on the recipe: Singularity.sambamba_0.7.1
## Local build:
```bash
sudo singularity build sambamba_v0.7.1.sif Singularity.sambamba_0.7.1
```
## Get image help:

```bash
singularity run-help sambamba_v0.7.1.sif
```
### Default runscript: sambamba
## Usage:
```bash
./sambamba_v0.7.1.sif --help
```
or:
```bash
singularity exec sambamba_v0.7.1.sif sambamba --help
```
image singularity (V>=3.3) is automacly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml
can be pull (singularity version >=3.3) with:

```bash
singularity pull sambamba_v0.7.1.sif oras://registry.forgemia.inra.fr/singularity/bioinfo/sambamba_v0.7.1/sambamba_v0.7.1:latest
```




